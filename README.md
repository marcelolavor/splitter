# Splitter

Great perl solution to properly manipulate file transfering among folders and FTP servers (jointly with FileTransfer project) with content analysis in order to separate and redirect information through different files based on a million record file. 

These perl codes were used during technological migration from TDMA to GSM over several months in production environment differenciating each mobile number migration status to GSM at very close to real-time, avoiding lack of billing information and revenue lost.

The solution also present a very well formed folder structure in order to allow new carriers and other file types implementation. It can be easily changed based on parameters.

A highlight to the record pointer in memory implemented in Perl handling the million of mobile numbers listed to be read at realtime at every billing information received from a bundle of carriers. A huge process to be treated.