#!/usr/local/bin/perl -w

# ===============================================================================
#	MAIN Routine
#	Usage: joiner.pl carrier service [log_error] [Intel] [display_info]
#
#	Carrier:
#		Maxitel 	  = mxt
#		Tim Sul 	  = tcs
#		Tim Nordeste  = tnc
#
#	Service:
#		Provisioning  = prov
#		Billing		  = bill
#
#	Usage Example:
#	joiner.pl mxt prov (This command line will process Maxitel Provisioning files
#	joiner.pl tnc bill (This command line will process Tim Nordeste billing files
#
# ===============================================================================


MAIN: {

    push @INC, "/usr/local/lib/perl5/5.00502/Pod/";

	# Get line command parameters
	($Carrier,$Service, $Log_erro, $PC, $Display) = @ARGV;

	# Set default values
	$Carrier = $Carrier || "";
	$Service = $Service || "";
	$Log_erro = $Log_erro || "0";
	$PC = $PC || "0";
	$Display = $Display || "0";

	chomp($Carrier,$Service, $Log_erro, $PC);

	# Check Help call
	if (lc($Carrier) eq "help") {
	   &PrintHelp; 	
	   die "\n$!";
	}

	# Check parameters needed
	if ($Carrier eq "") { die "Parameter Carrier is missing\nType help as parameter for more detail.\n$!"};		
	if ($Service eq "") { die "Parameter Service is missing\nType help as parameter for more detail.\n$!"};

	# Get Application start time
	my $APPStartTime = time();

	# Get Application path
	&GetAPP_DIR;

	# Program General information
	print "\nProcess ID: \t\t $$\n" if $Display ==1;
	$0 = "Joiner";
	print "Program Name: \t\t $0\n" if $Display ==1;
	print "Application root dir: \t $APP_DIR\n" if $Display ==1;
	print "Parameters \t\t $Carrier, $Service\n" if $Display ==1;

	# Check if is provisioning or billing
	if (lc($Service) eq "ret") {

		# Define service root dir	
		$Service_DIR = "$APP_DIR/prov/$Carrier/ret";

		# Define file types
		@Local_inputs = ("/acotel", "/wysdom");

		# Define output dirs
		@Output_dirs = ("/output");
		
		# Define the file extension
		$File_ext = "ret";

		print "Process type:\t\t Provisioning(RET)\n" if $Display ==1;

	} elsif (lc($Service) eq "sts") {

		# Define service root dir	
		$Service_DIR = "$APP_DIR/prov/$Carrier/sts";

		# Define file types
		@Local_inputs = ("/acotel", "/wysdom");

		# Define output dirs
		@Output_dirs = ("/output");

		# Define the file extension
		$File_ext = "sts";

		print "Process type:\t\t Provisioning(STS)\n" if $Display ==1;

	} elsif (lc($Service) eq "rqt") {

		# Define service root dir	
		$Service_DIR = "$APP_DIR/bill/$Carrier/rqt";	

		# Define file types
		@Local_inputs = ("/acotel/BLLRQTPRE", "/wysdom/BLLRQTPRE","/acotel/BLLRQTPOS", "/wysdom/BLLRQTPOS");

		# Define output dirs
		@Output_dirs = ("/output/BLLRQTPRE","/output/BLLRQTPOS");

		# Define the file extension
		$File_ext = "rqt";

		print "Process type:\t\t Billing(RQT)\n" if $Display ==1;

	} else {
		die "Unknow Parameter Service \nType help as parameter for more detail.\n$!"
	}

	# Define input data directory
    $Input_dir = "$Service_DIR/input";	# Files Local directory to be splitted

	# Convert to PC if necessary
	if ($PC==1) {
		$Service_DIR = &PCRunning($Service_DIR);
		$Input_dir = &PCRunning($Input_dir);
	}

	my $ERROR_FILE = "$Service_DIR/logs/logerro.txt";  #Error File

	# Convert to PC if necessary
	if ($PC==1) {
		$ERROR_FILE = &PCRunning($ERROR_FILE);
	}

	print "Service dir: \t\t $Service_DIR\n" if $Display ==1;
	print "Input dir: \t\t $Input_dir\n" if $Display ==1;	
	print "Error Log file: \t $ERROR_FILE\n" if $Display ==1;

	# Set errorlog file name
	if ($Log_erro) { open (STDERR,">>" . $ERROR_FILE) or die "Can't write to logfile: $!\n" };

	# Reset the accumulator of amount of files processed
	$Amount_File = 0;

	# Check input directories array
	for ($i = 0; $i <= $#{@Local_inputs}; $i=$i+2) {

		my $First_dir_name = $Input_dir . $Local_inputs[$i];
		my $Output_dir_name = $Service_DIR . $Output_dirs[$i/2];

		# Convert to PC if necessary
		if ($PC==1) {
			$First_dir_name = &PCRunning($First_dir_name);
			$Output_dir_name = &PCRunning($Output_dir_name);
		}

		print "First input dir: \t $First_dir_name\n" if $Display ==1;	
	
	    # OPEN EACH ".OPR" or ".RSP" FILE
	    opendir (DIR, $First_dir_name) or die "Cannot open directory: " . First_dir_name;

	    foreach my $file (readdir(DIR)) {

			print "File checking:\t\t $file\n" if $Display ==1;
	    	if ((!-d $file) && (lc(&FileExtension($file)) eq $File_ext)) {

				# Set the second file directory
				my $Second_dir_name = $Input_dir . $Local_inputs[$i+1];

				# Set the both file names
				my $First_file_name = $First_dir_name . "/" . $file;
				my $Second_file_name = $Second_dir_name . "/" . $file;
				my $Output_file_name = $Output_dir_name . "/" . $file;

				# Convert to PC if necessary
				if ($PC==1) {
					$First_file_name = &PCRunning($First_file_name);
					$Second_file_name = &PCRunning($Second_file_name);
					$Output_file_name = &PCRunning($Output_file_name);
				}

				print "First file: \t $First_file_name\n" if $Display ==1;	
				print "Second file: \t $Second_file_name\n" if $Display ==1;	
				print "Output file: \t $Output_file_name\n" if $Display ==1;	

				# Check if exist this file in the second dir				
				if (-e $Second_file_name) {
					my $StartTime = time();
					&JoinFile($First_file_name, $Second_file_name, $Output_file_name);
					print "Execution (seg):\t" . &TimeDiff($StartTime). "\n" if $Display ==1;
				}
	    	}
	    }

		# Close directory
	    closedir (DIR);
	}

	# Show Application summary
	print "\nFiles processed:\t$ Amount_File\n" if $Display ==1;
	print "Execution time(seg):\t" . &TimeDiff($APPStartTime). "\n" if $Display ==1;

	# Close all the files opened
	close CONTROL_FILE;
	close STDERR;
}


# ===============================================================================
#
#	JoinFile Function
#
# ===============================================================================
sub JoinFile {

	# GET PARAMETER
	my($First_file_name)=shift;
	my($Second_file_name)=shift;
	my($Output_file_name)=shift;

	# Open output file for join
	open (OUTPUT_FILE,">$Output_file_name") or die "Can't open file $Output_file_name\n$!";

	# First file process	
	open (FIRST_FILE,"$First_file_name") or die "Can't open file $First_file_name\n$!";
	$/=undef;
	my $data=<FIRST_FILE>;
	$/="\n";
	print OUTPUT_FILE $data or die "Can't write the first part to file $Output_file_name\n$!";
	close FIRST_FILE or die "Can't close file $First_file_name\n$!";

	# Second file process	
	open (SECOND_FILE,"$Second_file_name") or die "Can't open file $Second_file_name\n$!";
	$/=undef;
	$data=<SECOND_FILE>;
	$/="\n";
	print OUTPUT_FILE $data or die "Can't write the second part to file $Output_file_name\n$!";
	close SECOND_FILE or die "Can't close file $Second_file_name\n$!";

	# Close output file	
	close OUTPUT_FILE or die "Can't close file $OUTPUT_FILE\n$!";

	my $CARRIER_BACKUP_FILE_NAME = "$Service_DIR/backup/$Pre_pos_dir/$FileName." . &GetID;

	# Increment the amount of files processed
	++$Amount_File;

}


# ===============================================================================
#
#	FileName Function
#
# ===============================================================================
sub FileName {

	# GET PARAMETER
	my($FILE_NAME)=@_;
	my($file_name,$ext) = split(/\./,$FILE_NAME);
	return($file_name);
}

# ===============================================================================
#
#	FileExtension Function
#
# ===============================================================================
sub FileExtension {

	# GET PARAMETER
	my($FILE_NAME)=@_;
	my($file_name,$ext) = split(/\./,$FILE_NAME);
	return($ext);
}

# ===============================================================================
#
#	GetTimeArray Function
#
# ===============================================================================
sub GetTimeArray {

	my($sec,$min,$hour,$day,$mon,$year) = localtime(time()-24*3600);
	$year = $year+1900;
	$mon = $mon + 1;
	$day = $day +1;
	return($sec,$min,$hour,$day,$mon,$year);
}


# ===============================================================================
#
#	GetID Function
#
# ===============================================================================
sub GetID {

	#	GetTimeArray(Sec, Min, Hour, Day, Month, Year)
	my(@TimeArray) = &GetTimeArray;
	my($EXEC_TIME) = join("",$TimeArray[4],$TimeArray[3], $TimeArray[2], $TimeArray[1], $TimeArray[0]);
	return($EXEC_TIME);
}

# ===============================================================================
#
#	GetTime Function
#
# ===============================================================================
sub GetTime {

	#	GetTimeArray(Sec[0]), Min[1], Hour[2], Day[3], Month[4], Year[5])
	my(@TimeArray) = &GetTimeArray;
	my($EXEC_TIME) = "$TimeArray[5]/$TimeArray[4]/$TimeArray[3] $TimeArray[2]:$TimeArray[1]:$TimeArray[0]";
	return($EXEC_TIME);
}

# ===============================================================================
#
#	GetTimeArray Function
#
# ===============================================================================
sub TimeDiff {

	my $StartTime = shift;
	my $FinalTime = time();
	my $Diff = $FinalTime - $StartTime;

	#print $StartTime . "\n";
	#print $FinalTime . "\n";
	#print $Diff . "\n";

	return($Diff);
}

# ===============================================================================
#
#	PrintHelp Function
#
# ===============================================================================
sub PrintHelp {

print "\n
# ==============================================================
# Script Help
# Usage: joiner.pl carrier service [log_error] [Intel] [display_info]
#
# Carrier:
# 	Maxitel\t\t= mxt
# 	Tim Sul\t\t= tcs
# 	Tim Nordeste\t= tnc
#
# Service:
#	 Provisioning\t= prov
#	 Billing\t= bill
#
# Usage Examples:
#
# joiner.pl mxt prov
# (This command line will process Maxitel Provisioning files)
#
# joiner.pl tnc bill
# (This command line will process Tim Nordeste billing files)
#
# =============================================================\n";

}

# ===============================================================================
#
#	LogErro Function
#
# ===============================================================================
sub LogErro {
	$Err_msg = shift;
	open (ERROR_LOG,">>$Service_DIR/logs/logerro.txt") or die "Can't open logerro.txt file\n$!";
	print ERROR_LOG "$Err_msg\n$!";
	return "\n";
	open (STDERR,">$Service_DIR/logs/logerro.txt") or die "Can't write to logfile: $!\n";

}


# ===============================================================================
#
#	GetAPP_DIR Function
#
# ===============================================================================
sub GetAPP_DIR {

	if ($0=~m#^(.*)\\#) {
	    $cgi_dir = "$1";
	} elsif ($0=~m#^(.*)/# ) {
	    $cgi_dir = "$1";
	} else  {"pwd" =~ /(.*)/;
	    $cgi_dir = "$1";
	}

	if ($PC) {
		$cgi_dir = "c:\\perl\\dev\\splitter\\bin";
	}
	
	$APP_DIR = $cgi_dir . "/..";
	if ($PC) {
		$APP_DIR = &PCRunning($APP_DIR);
	}

	return($APP_DIR);
}


# ===============================================================================
#
#	PCRunning Function
#
# ===============================================================================
sub PCRunning {

	my $PC_Dir = shift;
	while ($PC_Dir =~ s/\//\\/) {
		$PC_Dir =~ s/\//\\/;
	}
	return $PC_Dir;
}
