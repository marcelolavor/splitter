#!/usr/local/bin/perl

# ===============================================================================
#	MAIN Routine
#	Usage: restore.pl carrier service [Intel] [display_info]
#
#	Carrier:
#		Maxitel 	  = mxt
#		Tim Sul 	  = tcs
#		Tim Nordeste  = tnc
#
#	Service:
#		Provisioning  = prov
#		Billing		  = bill
#
#	Usage Example:
#	spliter.pl mxt prov (This command line will restore Maxitel Provisioning files
#	spliter.pl tnc bill (This command line will restore Tim Nordeste billing files
#
# ===============================================================================

MAIN: {

	# Get line command parameters
	($Carrier,$Service, $PC, $Display) = @ARGV;

	# Get Application path
	&GetAPP_DIR;

	# Set default values
	$Carrier = $Carrier || "";
	$Service = $Service || "";
	$PC = $PC || "0";
	$Display = $Display || "0";

	chomp($Carrier,$Service, $PC, $Display);

	# Check Help call
	if (lc($Carrier) eq "help") {
	   &PrintHelp; 	
	   die "\n$!";
	}
	if ($Carrier eq "") { die "Parameter Carrier is missing\nType help as parameter for more detail.\n$!"};		
	if ($Service eq "") { die "Parameter Service is missing\nType help as parameter for more detail.\n$!"};
	
	print "Parameters \t\t $Carrier, $Service\n" if $Display ==1;

	# Program General information
	print "\nProcess ID: \t\t $$\n" if $Display ==1;
	$0 = "Restore_Splitter" if $Display ==1;
	print "Program Name: \t\t $0\n" if $Display ==1;
	print "Application root dir: \t $APP_DIR\n" if $Display ==1;

	
	# Define service root dir
	if (lc($Service) eq "prov") {

		# Define service root dir	
		$Service_DIR = "$APP_DIR/prov/$Carrier/opr";

		# Define file types
		@Pre_pos = ("/pre-pago", "/pos-pago");

		# Define the file extension
		$File_ext = "opr";

		print "Restore type:\t\t Provisioning\n" if $Display ==1;

	} else {

		# Define service root dir	
		$Service_DIR = "$APP_DIR/bill/$Carrier/rsp";	

		# Define file types
		@Pre_pos = ("/BLLRSPPRE", "/BLLRSPPOS");

		# Define the file extension
		$File_ext = "rsp";

		print "Restore type:\t\t Billing\n" if $Display ==1;
	}

	# Define input data directory
    local($Input_DIR) = "$Service_DIR/backup";

	# Convert to PC if necessary
	if ($PC==1) {
		$Service_DIR = &PCRunning($Service_DIR);
		$Input_DIR = &PCRunning($Input_DIR);
	}

	print "Service DIR \t\t $Service_DIR\n" if $Display ==1;
	print "Input DIR \t\t $Input_DIR\n" if $Display ==1;

	# Execute both pre-pago and pos-pago files
	foreach $Pre_pos_dir (@Pre_pos) {

		# Convert to PC if necessary
		if ($PC==1) {
			$Pre_pos_dir = &PCRunning($Pre_pos_dir);
		}

		print "Pre/Pos dir: \t\t $Pre_pos_dir\n\n" if $Display ==1;	
		    
	    # OPEN EACH ".OPR" FILE
	    opendir (DIR, $Input_DIR . $Pre_pos_dir) or die "Cannot open directory: " . $Input_DIR . $Pre_pos_dir . "n\$!";

	    foreach my $file (readdir(DIR)) {
			print "File checking:\t\t $file\n" if $Display ==1;
	    	if (!-d $file) {
	    		&RestoreFile($file);
	    	}
	    }
		# Close directory
	    closedir (DIR);
	}
}


# ===============================================================================
#
#	RestoreFile Function
#
# ===============================================================================
sub RestoreFile {

	# GET PARAMETER
	my($CARRIER_FILE_NAME)=shift;

	# Get Original file name
	my $FileName = &FileName($CARRIER_FILE_NAME);

	# Get Original file extention
	if (lc($Service) eq "prov") {
	   $File_EXT = "opr";
	} else {
	   $File_EXT = "rsp";
	}

	# Define output directories and files
	local $ACOTEL_FILE = "$Service_DIR/output/acotel/$Pre_pos_dir/$FileName.$File_EXT";
	local $TIMNET_FILE = "$Service_DIR/output/wysdom/$Pre_pos_dir/$FileName.$File_EXT";

	# Convert to PC if necessary
	if ($PC==1) {
		$ACOTEL_FILE = &PCRunning($ACOTEL_FILE);
		$TIMNET_FILE = &PCRunning($TIMNET_FILE);
	}

	print "------------------------------------------------------------------------\n" if $Display ==1;
	print "File name:\t\t $CARRIER_FILE_NAME\n" if $Display ==1;
	print "------------------------------------------------------------------------\n" if $Display ==1;

	unlink $ACOTEL_FILE;
	#print "File $ACOTEL_FILE deleted!\n";

	unlink $TIMNET_FILE;
	#print "File $TIMNET_FILE deleted!\n";

	# Set Original file name
	my $Original_File_Name = $Service_DIR ."/input$Pre_pos_dir/". $FileName .".". $File_EXT;

	# Convert to PC if necessary
	if ($PC==1) {
		$Original_File_Name = &PCRunning($Original_File_Name);
	}

	#print "Original file name => $Original_File_Name";

	my $Source_file = $Input_DIR ."$Pre_pos_dir/". $CARRIER_FILE_NAME;
	my $Target_file = $Original_File_Name;

	# Convert to PC if necessary
	if ($PC==1) {
		$Source_file = &PCRunning($Source_file);
		$Target_file = &PCRunning($Target_file);
	}

	# Rename backup file to original path and name
	rename($Source_file, $Target_file ) or die "Can't rename $CARRIER_FILE_NAME to $Original_File_Name\n$!";

}


# ===============================================================================
#
#	FileName Function
#
# ===============================================================================
sub FileName {

	# GET PARAMETER
	my($FILE_NAME)=@_;
	my($file_name,$ext) = split(/\./,$FILE_NAME);
	return($file_name);
}


# ===============================================================================
#
#	FileExtension Function
#
# ===============================================================================
sub FileExtension {

	# GET PARAMETER
	my($FILE_NAME)=@_;
	my($file_name,$ext) = split(/\./,$FILE_NAME);
	return($ext);
}

# ===============================================================================
#
#	PrintHelp Function
#
# ===============================================================================
sub PrintHelp {

print "\n
# ==============================================================
# Script Help
# Usage: restore.pl carrier service [Intel] [display_info]
#
# Carrier:
# 	Maxitel\t\t= mxt
# 	Tim Sul\t\t= tcs
# 	Tim Nordeste\t= tnc
#
# Service:
#	 Provisioning\t= prov
#	 Billing\t= bill
#
# Usage Examples:
#
# spliter.pl mxt prov
# (This command line will restore Maxitel Provisioning files)
#
# spliter.pl tnc bill
# (This command line will restore Tim Nordeste billing files)
#
# =============================================================\n\n";

}


# ===============================================================================
#
#	GetAPP_DIR Function
#
# ===============================================================================
sub GetAPP_DIR {

	if ($0=~m#^(.*)\\#) {
	    $cgi_dir = "$1";
	} elsif ($0=~m#^(.*)/# ) {
	    $cgi_dir = "$1";
	} else  {"pwd" =~ /(.*)/;
	    $cgi_dir = "$1";
	}

	if ($PC==1) {
		$cgi_dir = "c:\\perl\\dev\\splitter\\bin";
	}
	
	$APP_DIR = $cgi_dir . "/..";
	if ($PC==1) {
		$APP_DIR = &PCRunning($APP_DIR);
	}

	return($APP_DIR);
}


# ===============================================================================
#
#	PCRunning Function
#
# ===============================================================================
sub PCRunning {

	my $PC_Dir = shift;
	while ($PC_Dir =~ s/\//\\/) {
		$PC_Dir =~ s/\//\\/;
	}
	return $PC_Dir;
}
