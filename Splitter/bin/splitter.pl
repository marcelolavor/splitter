#!/usr/local/bin/perl -w

# ===============================================================================
#	MAIN Routine
#	Usage: spliter.pl carrier service [log_error] [Intel] [display_info]
#
#	Carrier:
#		Maxitel 	  = mxt
#		Tim Sul 	  = tcs
#		Tim Nordeste  = tnc
#
#	Service:
#		Provisioning  = prov
#		Billing		  = bill
#
#	Usage Example:
#	spliter.pl mxt prov (This command line will process Maxitel Provisioning files
#	spliter.pl tnc bill (This command line will process Tim Nordeste billing files
#
# ===============================================================================


MAIN: {

    push @INC, "/usr/local/lib/perl5/5.00502/Pod/";

	# Get line command parameters
	($Carrier,$Service, $Log_erro, $PC, $Display) = @ARGV;

	# Set default values
	$Carrier = $Carrier || "";
	$Service = $Service || "";
	$Log_erro = $Log_erro || "0";
	$PC = $PC || "0";
	$Display = $Display || "0";

	chomp($Carrier,$Service, $Log_erro, $PC);

	# Check Help call
	if (lc($Carrier) eq "help") {
	   &PrintHelp; 	
	   die "\n$!";
	}

	# Check parameters needed
	if ($Carrier eq "") { die "Parameter Carrier is missing\nType help as parameter for more detail.\n$!"};		
	if ($Service eq "") { die "Parameter Service is missing\nType help as parameter for more detail.\n$!"};

	# Get Application start time
	my $APPStartTime = time();

	# Get Application path
	&GetAPP_DIR;

	# Program General information
	print "\nProcess ID: \t\t $$\n" if $Display ==1;
	$0 = "Splitter";
	print "Program Name: \t\t $0\n" if $Display ==1;
	print "Application root dir: \t $APP_DIR\n" if $Display ==1;
	print "Parameters \t\t $Carrier, $Service\n" if $Display ==1;

	# Check if is provisioning or billing
	if (lc($Service) eq "prov") {

		# Define service root dir	
		$Service_DIR = "$APP_DIR/prov/$Carrier/opr";

		# Define input data directory
	    $Input_dir = "$Service_DIR/input";	# Files Local directory to be splitted

		# Define file types
		@Pre_pos = ("/pre-pago", "/pos-pago");

		# Define the file extension
		$File_ext = "opr";

		print "Process type:\t\t Provisioning\n" if $Display ==1;

	} else {

		# Define service root dir	
		$Service_DIR = "$APP_DIR/bill/$Carrier/rsp";	

		# Define input data directory
	    $Input_dir = "$Service_DIR/input";	# Files Local directory to be splitted

		# Define file types
		@Pre_pos = ("/BLLRSPPRE", "/BLLRSPPOS");

		# Define the file extension
		$File_ext = "rsp";

		print "Process type:\t\t Billing\n" if $Display ==1;

	}

	# Convert to PC if necessary
	if ($PC==1) {
		$Service_DIR = &PCRunning($Service_DIR);
		$Input_dir = &PCRunning($Input_dir);
	}

	my $ERROR_FILE = "$Service_DIR/logs/logerro.txt";  #Error File
	my $CONTROL_FILE_NAME = "$APP_DIR/control_lists/" . $Carrier . "_control_list.txt"; 	# Load Control

	# Convert to PC if necessary
	if ($PC) {
		$ERROR_FILE = &PCRunning($ERROR_FILE);
		$CONTROL_FILE_NAME = &PCRunning($CONTROL_FILE_NAME);
	}

	print "Service dir: \t\t $Service_DIR\n" if $Display ==1;
	print "Input dir: \t\t $Input_dir\n" if $Display ==1;	
	print "Error Log file: \t $ERROR_FILE\n" if $Display ==1;
	print "Control file: \t\t $CONTROL_FILE_NAME\n\n" if $Display ==1;

	# Set errorlog file name
	if ($Log_erro) { open (STDERR,">>" . $ERROR_FILE) or die "Can't write to logfile: $!\n" };

	# Open Control file with all mobile numbers that will be used to split
    open (CONTROL_FILE,$CONTROL_FILE_NAME) or die "Can't open file $CONTROL_FILE_NAME\n$!";
	@Control_List = <CONTROL_FILE>;

	# Reset the accumulator of amount of files processed
	$Amount_File = 0;

	# Execute both pre-pago and pos-pago files
	foreach $Pre_pos_dir (@Pre_pos) {

		# Convert to PC if necessary
		if ($PC==1) {
			$Pre_pos_dir = &PCRunning($Pre_pos_dir);
		}
	
		print "Pre/Pos dir: \t\t $Pre_pos_dir\n" if $Display ==1;	
	
	    # OPEN EACH ".OPR" or ".RSP" FILE
	    opendir (DIR, $Input_dir . $Pre_pos_dir) or die "Cannot open directory: " . $Input_dir . $Pre_pos_dir;

	    foreach my $file (readdir(DIR)) {
			print "File checking:\t\t $file\n" if $Display ==1;
	    	if ((!-d $file) && (lc(&FileExtension($file)) eq $File_ext)) {
				my $StartTime = time();
				my $File_size = -s $file || "0";
				&SplitFile($file);
				print "File size:\t\t $File_size (bytes)\n";
				print "Execution (seg):\t " . &TimeDiff($StartTime). "\n" if $Display ==1;
	    	}
	    }
		# Close directory
	    closedir (DIR);
	}

	# Show Application summary
	print "\nFiles processed:\t$ Amount_File\n" if $Display ==1;
	print "Execution time(seg):\t" . &TimeDiff($APPStartTime). "\n" if $Display ==1;

	# Close all the files opened
	close CONTROL_FILE;
	close STDERR;
}


# ===============================================================================
#
#	SplitFile Function
#
# ===============================================================================
sub SplitFile {

	# GET PARAMETER
	my($CARRIER_FILE_NAME)=shift;

	# Define output directories and files
	my $ACOTEL_File_opened = 0;
	my $TIMNET_File_opened = 0;
	my $FileName = &FileName($CARRIER_FILE_NAME);

	my $ACOTEL_FILE = "$Service_DIR/output/acotel$Pre_pos_dir/$CARRIER_FILE_NAME";
	my $TIMNET_FILE = "$Service_DIR/output/wysdom$Pre_pos_dir/$CARRIER_FILE_NAME";

	my $CARRIER_SOURCE_FILE_NAME = $Input_dir . $Pre_pos_dir ."/". $CARRIER_FILE_NAME;
	my $CARRIER_BACKUP_FILE_NAME = "$Service_DIR/backup$Pre_pos_dir/$FileName." . &GetID;

	# Convert to PC if necessary
	if ($PC) {
		$ACOTEL_FILE = &PCRunning($ACOTEL_FILE);
		$TIMNET_FILE = &PCRunning($TIMNET_FILE);
		$CARRIER_SOURCE_FILE_NAME = &PCRunning($CARRIER_SOURCE_FILE_NAME);
		$CARRIER_BACKUP_FILE_NAME = &PCRunning($CARRIER_BACKUP_FILE_NAME);
	}

	print "------------------------------------------------------------------------\n" if $Display ==1;
	print "File name:\t$CARRIER_FILE_NAME\n" if $Display ==1;
	print "Source file:\t$CARRIER_SOURCE_FILE_NAME\n" if $Display ==1;
	print "Backup file:\t$CARRIER_BACKUP_FILE_NAME\n" if $Display ==1;
	print "Target Acotel:\t$ACOTEL_FILE\n" if $Display ==1;
	print "Target Wysdom:\t$TIMNET_FILE\n" if $Display ==1;
	print "------------------------------------------------------------------------\n" if $Display ==1;

	open (CARRIER_FILE,$CARRIER_SOURCE_FILE_NAME) or die "Can't open file $CARRIER_FILE_NAME\n$!";
	while (<CARRIER_FILE>) {

        # Keep original registry 	  	
        my $OriginalCarrierLine = $_; 

		# Remove the split character from CARRIER_FILE line (DON'T REMOVE FROM "OriginalCarrierLine")
		chomp;
	
		my($status,$phone)=split(/\s+/);
		#print "PHONE TO CHECK: $phone\n\n";

		# Remove any space
		chomp($status,$phone);

		# Try to find this phone just if it is not null
		if ($phone ne "") {

    		my $FOUND = 0;		 	
			if (grep /$phone/, @Control_List) {
			   $FOUND=1;
			}
			    	
    		if ($FOUND==1) {
    		   if ($ACOTEL_File_opened == 0) {
    		   	  open (ACOTEL_FILE,">$ACOTEL_FILE") or die "Can't open file $ACOTEL_FILE\n$!";
    			  $ACOTEL_File_opened = 1;
    		    }		
				# Write the line in the target file
    			print ACOTEL_FILE $OriginalCarrierLine or die "Can't write to file $ACOTEL_FILE\n$!";
    			$FOUND = 0;
    		} else {
    		   if ($TIMNET_File_opened == 0) {
    		   	  open (TIMNET_FILE,">$TIMNET_FILE") or die "Can't open file $TIMNET_FILE\n$!";
    			  $TIMNET_File_opened = 1;
    		   }		
				# Write the line in the target file
    		   print TIMNET_FILE $OriginalCarrierLine or die "Can't write to file $TIMNET_FILE\n$!";
    		}
		}	
	}

	# Close Input File
	close CARRIER_FILE or die "Can't close file $CARRIER_FILE\n$!";

	# Close Output Files
    if ($ACOTEL_File_opened == 1) {
	   close ACOTEL_FILE or die "Can't close file $ACOTEL_FILE\n$!";
	}		
    if ($ACOTEL_File_opened == 1) {
	   close TIMNET_FILE or die "Can't close file $TIMNET_FILE\n$!";
	}

	# Increment the amount of files processed
	++$Amount_File;

	rename($CARRIER_SOURCE_FILE_NAME, "$CARRIER_BACKUP_FILE_NAME") or die "Can't rename $CARRIER_SOURCE_FILE_NAME to $CARRIER_BACKUP_FILE_NAME\n$!";
	print "Backup of $CARRIER_FILE_NAME done\n\n" if $Display ==1;	
}


# ===============================================================================
#
#	FileName Function
#
# ===============================================================================
sub FileName {

	# GET PARAMETER
	my($FILE_NAME)=@_;
	my($file_name,$ext) = split(/\./,$FILE_NAME);
	return($file_name);
}

# ===============================================================================
#
#	FileExtension Function
#
# ===============================================================================
sub FileExtension {

	# GET PARAMETER
	my($FILE_NAME)=@_;
	my($file_name,$ext) = split(/\./,$FILE_NAME);
	return($ext);
}

# ===============================================================================
#
#	GetTimeArray Function
#
# ===============================================================================
sub GetTimeArray {

	my($sec,$min,$hour,$day,$mon,$year) = localtime(time()-24*3600);
	$year = $year+1900;
	$mon = $mon + 1;
	$day = $day +1;
	return($sec,$min,$hour,$day,$mon,$year);
}


# ===============================================================================
#
#	GetID Function
#
# ===============================================================================
sub GetID {

	#	GetTimeArray(Sec, Min, Hour, Day, Month, Year)
	my(@TimeArray) = &GetTimeArray;
	my($EXEC_TIME) = join("",$TimeArray[4],$TimeArray[3], $TimeArray[2], $TimeArray[1], $TimeArray[0]);
	return($EXEC_TIME);
}

# ===============================================================================
#
#	GetTime Function
#
# ===============================================================================
sub GetTime {

	#	GetTimeArray(Sec[0]), Min[1], Hour[2], Day[3], Month[4], Year[5])
	my(@TimeArray) = &GetTimeArray;
	my($EXEC_TIME) = "$TimeArray[5]/$TimeArray[4]/$TimeArray[3] $TimeArray[2]:$TimeArray[1]:$TimeArray[0]";
	return($EXEC_TIME);
}

# ===============================================================================
#
#	GetTimeArray Function
#
# ===============================================================================
sub TimeDiff {

	my $StartTime = shift;
	my $FinalTime = time();
	my $Diff = $FinalTime - $StartTime;

	#print $StartTime . "\n";
	#print $FinalTime . "\n";
	#print $Diff . "\n";

	return($Diff);
}

# ===============================================================================
#
#	PrintHelp Function
#
# ===============================================================================
sub PrintHelp {

print "\n
# ==============================================================
# Script Help
# Usage: spliter.pl carrier service [log_error] [Intel] [display_info]
#
# Carrier:
# 	Maxitel\t\t= mxt
# 	Tim Sul\t\t= tcs
# 	Tim Nordeste\t= tnc
#
# Service:
#	 Provisioning\t= prov
#	 Billing\t= bill
#
# Usage Examples:
#
# spliter.pl mxt prov
# (This command line will process Maxitel Provisioning files)
#
# spliter.pl tnc bill
# (This command line will process Tim Nordeste billing files)
#
# =============================================================\n";

}

# ===============================================================================
#
#	LogErro Function
#
# ===============================================================================
sub LogErro {
	$Err_msg = shift;
	open (ERROR_LOG,">>$Service_DIR/logs/logerro.txt") or die "Can't open logerro.txt file\n$!";
	print ERROR_LOG "$Err_msg\n$!";
	return "\n";
	open (STDERR,">$Service_DIR/logs/logerro.txt") or die "Can't write to logfile: $!\n";

}


# ===============================================================================
#
#	GetAPP_DIR Function
#
# ===============================================================================
sub GetAPP_DIR {

	if ($0=~m#^(.*)\\#) {
	    $cgi_dir = "$1";
	} elsif ($0=~m#^(.*)/# ) {
	    $ cgi_dir = "$1";
	} else  {`pwd` =~ /(.*)/;
	    $ cgi_dir = "$1";
	}

	if ($PC==1) {
		$cgi_dir = "c:\\perl\\dev\\splitter\\bin";
	}
	
	$APP_DIR = $cgi_dir . "/..";
	if ($PC==1) {
		$APP_DIR = &PCRunning($APP_DIR);
	}

	return($APP_DIR);
}


# ===============================================================================
#
#	PCRunning Function
#
# ===============================================================================
sub PCRunning {

	my $PC_Dir = shift;
	while ($PC_Dir =~ s/\//\\/) {
		$PC_Dir =~ s/\//\\/;
	}
	return $PC_Dir;
}
