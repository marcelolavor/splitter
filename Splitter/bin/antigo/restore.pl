#!/usr/local/bin/perl #-w

# ===============================================================================
#	MAIN Routine
#	Usage: restore.pl carrier service
#
#	Carrier:
#		Maxitel 	  = mxt
#		Tim Sul 	  = tcs
#		Tim Nordeste  = tnc
#
#	Service:
#		Provisioning  = prov
#		Billing		  = bill
#
#	Usage Example:
#	spliter.pl mxt prov (This command line will restore Maxitel Provisioning files
#	spliter.pl tnc bill (This command line will restore Tim Nordeste billing files
#
# ===============================================================================

MAIN: {

	# Get Application path
	&GetAPP_DIR;

	# Program General information
	print "Process ID: $$\n";
	print "Program Name: $0\n";
	print "Application root dir: $APP_DIR\n";

	# Get line command parameters
	($Carrier,$Service) = @ARGV;

	chomp;

	if (lc($Carrier) eq "help") {
	   &PrintHelp; 	
	   die "\n$!";
	}
	if ($Carrier eq "") { die "Parameter Carrier is missing\nType help as parameter for more detail.\n$!"};		
	if ($Service eq "") { die "Parameter Service is missing\nType help as parameter for more detail.\n$!"};
	
	print "Parameters =>  Carrier:$Carrier   Service: $Service\n";
	
	# Define service root dir
	if (lc($Service) eq "prov") {
	   $Service_DIR = "$APP_DIR/prov/$Carrier/opr";
	} else {
	   $Service_DIR = "$APP_DIR/bill/$Carrier/rsp";	
	}

	print "Service DIR => $Service_DIR\n";
	
	# Define input data directory
    local($Input_DIR) = "$Service_DIR/backup";

	print "Input DIR => $Input_DIR\n";
	    
    # OPEN EACH ".OPR" FILE
    opendir (DIR, $Input_DIR) or die "Cannot open directory: " . $Input_DIR ."n\$!";
    foreach my $file (readdir(DIR)) {
    	if (!-d $file) {
    		&RestoreFile($file);
    	}
    }
	# Close directory
    closedir (DIR);
}


# ===============================================================================
#
#	RestoreFile Function
#
# ===============================================================================
sub RestoreFile {

	# GET PARAMETER
	my($CARRIER_FILE_NAME)=shift;

	# Get Original file name
	my $FileName = &FileName($CARRIER_FILE_NAME);

	# Get Original file extention
	if (lc($Service) eq "prov") {
	   $File_EXT = "opr";
	} else {
	   $File_EXT = "rsp";
	}

	# Define output directories and files
	local $ACOTEL_FILE = "$Service_DIR/output/acotel/$FileName.$File_EXT";
	local $TIMNET_FILE = "$Service_DIR/output/timnet/$FileName.$File_EXT";

	unlink $ACOTEL_FILE;
	#print "File $ACOTEL_FILE deleted!\n";

	unlink $TIMNET_FILE;
	#print "File $TIMNET_FILE deleted!\n";

	# Set Original file name
	my $Original_File_Name = $Service_DIR ."/input/". $FileName .".". $File_EXT;

	#print "Original file name => $Original_File_Name";

	# Rename backup file to original path and name
	rename($Input_DIR ."/". $CARRIER_FILE_NAME, $Original_File_Name ) or die "Can't rename $CARRIER_FILE_NAME to $Original_File_Name\n$!";

}


# ===============================================================================
#
#	FileName Function
#
# ===============================================================================
sub FileName {

	# GET PARAMETER
	my($FILE_NAME)=@_;
	my($file_name,$ext) = split(/\./,$FILE_NAME);
	return($file_name);
}


# ===============================================================================
#
#	PrintHelp Function
#
# ===============================================================================
sub PrintHelp {

print "\n
# ==============================================================
# Script Help
# Usage: restore.pl carrier service
#
# Carrier:
# 	Maxitel\t\t= mxt
# 	Tim Sul\t\t= tcs
# 	Tim Nordeste\t= tnc
#
# Service:
#	 Provisioning\t= prov
#	 Billing\t= bill
#
# Usage Examples:
#
# spliter.pl mxt prov
# (This command line will restore Maxitel Provisioning files)
#
# spliter.pl tnc bill
# (This command line will restore Tim Nordeste billing files)
#
# =============================================================\n\n";

}


# ===============================================================================
#
#	GetAPP_DIR Function
#
# ===============================================================================
sub GetAPP_DIR {

	if ($0=~m#^(.*)\\#) {
	    $cgi_dir = "$1";
	} elsif ($0=~m#^(.*)/# ) {
	    $ cgi_dir = "$1";
	} else  {`pwd` =~ /(.*)/;
	    $ cgi_dir = "$1";
	}

	$APP_DIR = $cgi_dir . "\\..";
	#$APP_DIR = "c:/perl/dev/splitter";
	print "App DIR => $APP_DIR\n";

}


# ===============================================================================
#
#	PCRunning Function
#
# ===============================================================================
sub PCRunning {

	my $PC_Dir = shift;
	$PC_Dir =~ s/c\://;
	$PC_Dir =~ s/\\/\//;
}