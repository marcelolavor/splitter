#!/usr/local/bin/perl #-w

# Define script directory
$APP_DIR = "c:/Perl/Dev/Splitter"

# Define Provisioning directories
$OPR_DIR = "$APP_DIR/Prov/mxt/opr";

# ===============================================================================
#
#	MAIN Routine
#
# ===============================================================================
MAIN: {

    my($dir) = "$OPR_DIR/input";
    
    # OPEN EACH ".OPR" FILE
    opendir (DIR, $dir) or die "Cannot open directory: $dir";
    foreach my $file (readdir(DIR)) {
    	if ((!-d $file) && (lc(&FileExtension($file)) eq lc("opr"))) {
    		&SplitFile($file);
    	}
    }
    closedir (DIR);
}



# ===============================================================================
#
#	SplitFile Function
#
# ===============================================================================
sub SplitFile {

	# GET PARAMETER
	my($CARRIER_FILE_NAME)=@_;

	local $ACOTEL_FILE = "$OPR_DIR/input/acotel/$CARRIER_FILE_NAME";
	local $TIMNET_FILE = "$OPR_DIR/input/timnet/$CARRIER_FILE_NAME";
	my $FileName = &FileName($CARRIER_FILE_NAME);
	my $ExecTime = &GetTime;
	my $CARRIER_BACKUP_FILE_NAME = "$OPR_DIR/backup/$FileName.$ExecTime";
	my $CONTROL_FILE = "max_control_list.txt";
	
	#print "FILE NAME: $FileName\n";
	#print "ACOTEL FILE: $ACOTEL_FILE\n";
	#print "TIMNET FILE: $TIMNET_FILE\n";
	#print "EXECUTION TIME: $ExecTime\n";
	#print "BACKUP FILE: $CARRIER_BACKUP_FILE_NAME\n\n";

	open (CARRIER_FILE,$CARRIER_FILE_NAME) or die "Can't open file $CARRIER_FILE_NAME: $!";
	open (ACOTEL_FILE,">>$ACOTEL_FILE") or die "Can't open file $ACOTEL_FILE: $!";
	open (TIMNET_FILE,">>$TIMNET_FILE") or die "Can't open file $TIMNET_FILE: $!";

	while (<CARRIER_FILE>) {

        # Keep original registry 	  	
        my $OriginalCarrierLine = $_; 

		# Remove the split character from CARRIER_FILE line 	  	
		chomp;
	
		($status,$phone)=split(/\s+/);
		#print "PHONE TO CHECK: $phone\n\n";
	
		open (CONTROL_FILE,$CONTROL_FILE) or die "Can't open file $CONTROL_FILE: $!";
		#print "CONTROL PHONE LIST:\n\n";

		$FOUND = 0;
		while (<CONTROL_FILE>) {

    		# Remove the split character 	  	
			chomp;
			
			#($control_status,$control_phone)=split(/\s+/);
			#print $phone . "  " . $_ . "\n";
			if ($phone == $_) {
				$FOUND=1;
				last;
			}
		}

		#print "Found: $FOUND\n";
	
		if ($FOUND==1) {
			print ACOTEL_FILE $OriginalCarrierLine or die "Can't write to file $ACOTEL_FILE: $!";
			$FOUND = 0;
		} else {
			print TIMNET_FILE $OriginalCarrierLine or die "Can't write to file $TIMNET_FILE: $!";
		}
	
		close CONTROL_FILE;
	}
	close CARRIER_FILE or die "Can't close file $CARRIER_FILE: $!";
	close ACOTEL_FILE or die "Can't close file $ACOTEL_FILE: $!";
	close TIMNET_FILE or die "Can't close file $TIMNET_FILE: $!";
	
	
	rename($CARRIER_FILE_NAME, "$CARRIER_BACKUP_FILE_NAME") or die "Can't rename $CARRIER_FILE_NAME to $CARRIER_BACKUP_FILE_NAME: $!";
	
	#use strict;
	#use File::Copy;
	
	#my $source = "$CARRIER_BACKUP_FILE_NAME";
	#my $dest = "/backup/";
	
	#move($source, $dest) or die "Error moving file %CARRIER_BACKUP_FILE_NAME: $!\n";

	&FTPSend $ACOTEL_FILE;
}


# ===============================================================================
#
#	FileName Function
#
# ===============================================================================
sub FileName {

	# GET PARAMETER
	my($FILE_NAME)=@_;
	my($file_name,$ext) = split(/\./,$FILE_NAME);
	return($file_name);
}

# ===============================================================================
#
#	FileExtension Function
#
# ===============================================================================
sub FileExtension {

	# GET PARAMETER
	my($FILE_NAME)=@_;
	my($file_name,$ext) = split(/\./,$FILE_NAME);
	return($ext);
}


# ===============================================================================
#
#	GetTime Function
#
# ===============================================================================
sub GetTime {

	my($sec,$min,$hour,$day,$mon) = localtime($^T);
	my($EXEC_TIME) = join("",$mon+1,$day,$hour,$min,$sec);
	return($EXEC_TIME);
}



# ===============================================================================
#
#	FTPSend Function
#
# ===============================================================================
sub FTPSend {

	my $local = shift;

	use Net::FTP;
	use constant HOST => '127.0.0.1';
	use constant DIR  => '/';
	use constant FILE => 'readme.txt';

	my $ftp = Net::FTP->new(HOST) or die "Couldn't connect: $@\n";
	$ftp->login('mlavor','a15v32h')      or die $ftp->message;
	$ftp->cwd(DIR)                or die $ftp->message;
	@items = $ftp->ls('-a')	  or die $ftp->message;
	foreach $item (@items) {
		  print $item;
		  print "\n";
	}
	#$ftp->get(FILE,\*STDOUT)            or die $ftp->message;
	#$ftp->put($local [,$remote])
	$ftp->quit;

}