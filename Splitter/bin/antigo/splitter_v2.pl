#!/usr/local/bin/perl #-w

# ===============================================================================
#	MAIN Routine
#	Usage: spliter.pl carrier service
#
#	Carrier:
#		Maxitel 	  = mxt
#		Tim Sul 	  = tcs
#		Tim Nordeste  = tnc
#
#	Service:
#		Provisioning  = prov
#		Billing		  = bill
#
#	Usage Example:
#	spliter.pl mxt prov (This command line will process Maxitel Provisioning files
#	spliter.pl tnc bill (This command line will process Tim Nordeste billing files
#
# ===============================================================================

MAIN: {

	# Get line command parameters
	local($Carrier,$Service) = @ARGV;
	#local ($Carrier,$Service) = ("mxt","prov");

	chomp;

	if (lc($Carrier) eq "help") {
	   &PrintHelp; 	
	   die "\n$!";
	}
	if ($Carrier eq "") { die "Parameter Carrier is missing\nType help as parameter for more detail.\n$!"};		
	if ($Service eq "") { die "Parameter Service is missing\nType help as parameter for more detail.\n$!"};
	
	#print "Parameters =>  Carrier:$Carrier   Service: $Service\n";
	
	local $APP_DIR = "c:/perl/dev/splitter";
	#print "App DIR => $APP_DIR\n";

	# Define service root dir
	if (lc($Service) eq "prov") {
	   $Service_DIR = "$APP_DIR/prov/$Carrier/opr";
	} else {
	   $Service_DIR = "$APP_DIR/bill/$Carrier/rsp";	
	}

	#print "Service DIR => $Service_DIR\n";
	
	# Define input data directory
    local($Input_DIR) = "$Service_DIR/input";

	#print "Input DIR => $Input_DIR\n";	

	# Set errorlog file name
	open (STDERR,">>$Service_DIR/logs/logerro.txt") or die "Can't write to logfile: $!\n";
	print STDERR "Date: " . &GetTime;
	
    # OPEN EACH ".OPR" FILE
    opendir (DIR, $Input_DIR) or die "Cannot open directory: " . $Input_DIR;
    foreach my $file (readdir(DIR)) {
    	if ((!-d $file) && (lc(&FileExtension($file)) eq "opr")) {
    		&SplitFile($file);
    	}
    }
	# Close directory
    closedir (DIR);
}


# ===============================================================================
#
#	SplitFile Function
#
# ===============================================================================
sub SplitFile {

	# GET PARAMETER
	my($CARRIER_FILE_NAME)=shift;

	my $FileName = &FileName($CARRIER_FILE_NAME);
	#my $ExecTime = &GetID;
	my $CARRIER_BACKUP_FILE_NAME = "$Service_DIR/backup/$FileName." . &GetID;
	my $CONTROL_FILE = "$APP_DIR/control_lists/" . $Carrier . "_control_list.txt";

    local $ACOTEL_File_opened = 0;
    local $TIMNET_File_opened = 0;
	
	print "FILE NAME: $FileName\n";
	#print "CONTROL FILE: $CONTROL_FILE\n\n";

	open (CARRIER_FILE,$Input_DIR ."/". $CARRIER_FILE_NAME) or die "Can't open file $CARRIER_FILE_NAME\n$!";
	while (<CARRIER_FILE>) {

        # Keep original registry 	  	
        my $OriginalCarrierLine = $_; 

		# Remove the split character from CARRIER_FILE line 	  	
		chomp;
	
		my($status,$phone)=split(/\s+/);
		#print "PHONE TO CHECK: $phone\n\n";

		# Try to find this phone just if it is not null
		if ($phone ne "") {
		   #Check id $phone belongs to Control List
		   &CheckPhone($phone);		 	
		}	
	}

	# Close CARRIER_FILE
	close CARRIER_FILE or die "Can't close file $CARRIER_FILE\n$!";

    if ($ACOTEL_File_opened == 1) {
	   close ACOTEL_FILE or die "Can't close file $ACOTEL_FILE\n$!";
	}		
    if ($ACOTEL_File_opened == 1) {
	   close TIMNET_FILE or die "Can't close file $TIMNET_FILE\n$!";
	}

	rename($Input_DIR ."/". $CARRIER_FILE_NAME, "$CARRIER_BACKUP_FILE_NAME") or die "Can't rename $CARRIER_FILE_NAME to $CARRIER_BACKUP_FILE_NAME\n$!";
	#print "BACKUP FILE DONE: $CARRIER_BACKUP_FILE_NAME\n";	
}


# ===============================================================================
#
#	FileName Function
#
# ===============================================================================
sub FileName {

	# GET PARAMETER
	my($FILE_NAME)=@_;
	my($file_name,$ext) = split(/\./,$FILE_NAME);
	return($file_name);
}

# ===============================================================================
#
#	FileExtension Function
#
# ===============================================================================
sub FileExtension {

	# GET PARAMETER
	my($FILE_NAME)=@_;
	my($file_name,$ext) = split(/\./,$FILE_NAME);
	return($ext);
}


# ===============================================================================
#
#	GetID Function
#
# ===============================================================================
sub GetID {

	my($sec,$min,$hour,$day,$mon) = localtime(time()-24*3600);
	my($EXEC_TIME) = join("",$mon+1,$day,$hour,$min,$sec);
	return($EXEC_TIME);
}

# ===============================================================================
#
#	GetTime Function
#
# ===============================================================================
sub GetTime {

	my($sec,$min,$hour,$day,$mon,$year) = localtime(time()-24*3600);
	$year = $year+1899;
	$mon = $mon + 1;
	my($EXEC_TIME) = "$year/$mon/$day $hour:$min:$sec\n";
	return($EXEC_TIME);
}


# ===============================================================================
#
#	PrintHelp Function
#
# ===============================================================================
sub PrintHelp {

print "\n
# ==============================================================
# Script Help
# Usage: spliter.pl carrier service
#
# Carrier:
# 	Maxitel\t\t= mxt
# 	Tim Sul\t\t= tcs
# 	Tim Nordeste\t= tnc
#
# Service:
#	 Provisioning\t= prov
#	 Billing\t= bill
#
# Usage Examples:
#
# spliter.pl mxt prov
# (This command line will process Maxitel Provisioning files)
#
# spliter.pl tnc bill
# (This command line will process Tim Nordeste billing files)
#
# =============================================================\n\n";

}

# ===============================================================================
#
#	LogErro Function
#
# ===============================================================================
sub LogErro {
	$Err_msg = shift;
	open (ERROR_LOG,">>$Service_DIR/logs/logerro.txt") or die "Can't open logerro.txt file\n$!";
	print ERROR_LOG "$Err_msg\n$!";
	return "\n";
	open (STDERR,">$Service_DIR/logs/logerro.txt") or die "Can't write to logfile: $!\n";


}


# ===============================================================================
#
#	CheckPhone Function
#
# ===============================================================================
sub CheckPhone {

	   # Get Phone to check
		my $phone = shift;

        # Define output directories and files
        local $ACOTEL_FILE = "$Service_DIR/output/acotel/$CARRIER_FILE_NAME";
        local $TIMNET_FILE = "$Service_DIR/output/timnet/$CARRIER_FILE_NAME";

		my $FOUND = 0;
			
		open (CONTROL_FILE,$CONTROL_FILE) or die "Can't open file $CONTROL_FILE\n$!";
		#print "CONTROL PHONE LIST:\n\n";

		while (<CONTROL_FILE>) {

    		# Remove the split character 	  	
			chomp;
			
			#($control_status,$control_phone)=split(/\s+/);
			#print $phone . "  " . $_ . "\n";
			if ($phone == $_) {
				$FOUND=1;
				last;
			}
		}
 		close CONTROL_FILE;
		
		#print "Found: $FOUND\n";
	
		if ($FOUND==1) {
		   if ($ACOTEL_File_opened == 0) {
		   	  open (ACOTEL_FILE,">$ACOTEL_FILE") or die "Can't open file $ACOTEL_FILE\n$!";
			  $ACOTEL_File_opened = 1;
			  #print "ACOTEL FILE: $ACOTEL_FILE\n";
		    }		
			print ACOTEL_FILE $OriginalCarrierLine or die "Can't write to file $ACOTEL_FILE\n$!";
			$FOUND = 0;
		} else {
		   if ($TIMNET_File_opened == 0) {
		   	  open (TIMNET_FILE,">$TIMNET_FILE") or die "Can't open file $TIMNET_FILE\n$!";
			  $TIMNET_File_opened = 1;
			   #print "TIMNET FILE: $TIMNET_FILE\n";
		   }		
		   print TIMNET_FILE $OriginalCarrierLine or die "Can't write to file $TIMNET_FILE\n$!";
		}
}

# ===============================================================================
#
#	CheckPhone Function
#
# ===============================================================================
sub CheckPhone {

	   # Get Phone to check
		my $phone = shift;

		if ( (grep /\./, $$arrayRef[$i]) != "" )
}


# ===============================================================================
#
#	PCRunning Function
#
# ===============================================================================
sub PCRunning {

	my $PC_Dir = shift;
	$PC_Dir =~ s/c\://;
	$PC_Dir =~ s/\\/\//;
}
