#!/usr/local/bin/perl #-w

# ===============================================================================
#	MAIN Routine
#	Usage: spliter.pl carrier service
#
#	Carrier:
#		Maxitel 	  = mxt
#		Tim Sul 	  = tcs
#		Tim Nordeste  = tnc
#
#	Service:
#		Provisioning  = prov
#		Billing		  = bill
#
#	Usage Example:
#	spliter.pl mxt prov (This command line will process Maxitel Provisioning files
#	spliter.pl tnc bill (This command line will process Tim Nordeste billing files
#
# ===============================================================================

BEGIN {
    push @INC, "/usr/local/lib/perl5/5.00502/Pod/";
}


MAIN: {

	# Get line command parameters
	#local($Carrier,$Service) = @ARGV;
	local ($Carrier,$Service,$PC) = ("mxt","prov");

	chomp;

	if (lc($Carrier) eq "help") {
	   &PrintHelp; 	
	   die "\n$!";
	}
	if ($Carrier eq "") { die "Parameter Carrier is missing\nType help as parameter for more detail.\n$!"};		
	if ($Service eq "") { die "Parameter Service is missing\nType help as parameter for more detail.\n$!"};
	
	#print "Parameters =>  Carrier:$Carrier   Service: $Service\n";
	
	local $APP_DIR = "c:/perl/dev/splitter";
	#print "App DIR => $APP_DIR\n";

	# Define service root dir
	if (lc($Service) eq "prov") {
	   $Service_DIR = "$APP_DIR/prov/$Carrier/opr";
	} else {
	   $Service_DIR = "$APP_DIR/bill/$Carrier/rsp";	
	}

	#print "Service DIR => $Service_DIR\n";
	
	# Define input data directory
    local($Input_DIR) = "$Service_DIR/input";

	#print "Input DIR => $Input_DIR\n";	

	# Set errorlog file name
	open (STDERR,">>$Service_DIR/logs/logerro.txt") or die "Can't write to logfile: $!\n";
	#print STDERR "Date: " . &GetTime;

	# Load Control 
	my $CONTROL_FILE = "$APP_DIR/control_lists/" . $Carrier . "_control_list.txt";
    open (CONTROL_FILE,$CONTROL_FILE) or die "Can't open file $CONTROL_FILE\n$!";
	local @Control_List = <CONTROL_FILE>;
	
    # OPEN EACH ".OPR" FILE
    opendir (DIR, $Input_DIR) or die "Cannot open directory: " . $Input_DIR;
    foreach my $file (readdir(DIR)) {
    	if ((!-d $file) && (lc(&FileExtension($file)) eq "opr")) {
		   my $StartTime = &GetTime; #time();
		   &SplitFile($file);
		   print &TimeDiff($StartTime) . "\n";
    	}
    }
	# Close directory
    closedir (DIR);
}


# ===============================================================================
#
#	SplitFile Function
#
# ===============================================================================
sub SplitFile {

	# GET PARAMETER
	my($CARRIER_FILE_NAME)=shift;

	# Define output directories and files
	local $ACOTEL_FILE = "$Service_DIR/output/acotel/$CARRIER_FILE_NAME";
	local $TIMNET_FILE = "$Service_DIR/output/timnet/$CARRIER_FILE_NAME";

	my $ACOTEL_File_opened = 0;
	my $TIMNET_File_opened = 0;
	my $FileName = &FileName($CARRIER_FILE_NAME);
	#my $ExecTime = &GetID;
	my $CARRIER_BACKUP_FILE_NAME = "$Service_DIR/backup/$FileName." . &GetID;
	
	print "FILE NAME: $FileName\n";
	#print "CONTROL FILE: $CONTROL_FILE\n\n";

	open (CARRIER_FILE,$Input_DIR ."/". $CARRIER_FILE_NAME) or die "Can't open file $CARRIER_FILE_NAME\n$!";
	while (<CARRIER_FILE>) {

        # Keep original registry 	  	
        my $OriginalCarrierLine = $_; 

		# Remove the split character from CARRIER_FILE line 	  	
		chomp;
	
		my($status,$phone)=split(/\s+/);
		#print "PHONE TO CHECK: $phone\n\n";

		# Try to find this phone just if it is not null
		if ($phone ne "") {

    		my $FOUND = 0;		 	
			if (grep /$phone/, @Control_List) {
			   $FOUND=1;
			}
			    	
    		if ($FOUND==1) {
    		   if ($ACOTEL_File_opened == 0) {
    		   	  open (ACOTEL_FILE,">$ACOTEL_FILE") or die "Can't open file $ACOTEL_FILE\n$!";
    			  $ACOTEL_File_opened = 1;
				  #print "ACOTEL FILE: $ACOTEL_FILE\n";
    		    }		
    			print ACOTEL_FILE $OriginalCarrierLine or die "Can't write to file $ACOTEL_FILE\n$!";
    			$FOUND = 0;
    		} else {
    		   if ($TIMNET_File_opened == 0) {
    		   	  open (TIMNET_FILE,">$TIMNET_FILE") or die "Can't open file $TIMNET_FILE\n$!";
    			  $TIMNET_File_opened = 1;
				  #print "TIMNET FILE: $TIMNET_FILE\n";
    		   }		
    		   print TIMNET_FILE $OriginalCarrierLine or die "Can't write to file $TIMNET_FILE\n$!";
    		}
		}	
		close CONTROL_FILE;
	}

	# Close all files
	close CARRIER_FILE or die "Can't close file $CARRIER_FILE\n$!";
    if ($ACOTEL_File_opened == 1) {
	   close ACOTEL_FILE or die "Can't close file $ACOTEL_FILE\n$!";
	}		
    if ($ACOTEL_File_opened == 1) {
	   close TIMNET_FILE or die "Can't close file $TIMNET_FILE\n$!";
	}

	rename($Input_DIR ."/". $CARRIER_FILE_NAME, "$CARRIER_BACKUP_FILE_NAME") or die "Can't rename $CARRIER_FILE_NAME to $CARRIER_BACKUP_FILE_NAME\n$!";
	#print "BACKUP FILE DONE: $CARRIER_BACKUP_FILE_NAME\n";	
}


# ===============================================================================
#
#	FileName Function
#
# ===============================================================================
sub FileName {

	# GET PARAMETER
	my($FILE_NAME)=@_;
	my($file_name,$ext) = split(/\./,$FILE_NAME);
	return($file_name);
}

# ===============================================================================
#
#	FileExtension Function
#
# ===============================================================================
sub FileExtension {

	# GET PARAMETER
	my($FILE_NAME)=@_;
	my($file_name,$ext) = split(/\./,$FILE_NAME);
	return($ext);
}

# ===============================================================================
#
#	GetTimeArray Function
#
# ===============================================================================
sub GetTimeArray {

	my($sec,$min,$hour,$day,$mon,$year) = localtime(time()-24*3600);
	$year = $year+1900;
	$mon = $mon + 1;
	$day = $day +1;
	return($sec,$min,$hour,$day,$mon,$year);
}


# ===============================================================================
#
#	GetID Function
#
# ===============================================================================
sub GetID {

	#	GetTimeArray(Sec, Min, Hour, Day, Month, Year)
	my(@TimeArray) = &GetTimeArray;
	my($EXEC_TIME) = join("",@TimeArray[4],@TimeArray[3], @TimeArray[2], @TimeArray[1], @TimeArray[0]);
	#my($EXEC_TIME) = join("",$mon+1,$day,$hour,$min,$sec);
	return($EXEC_TIME);
}

# ===============================================================================
#
#	GetTime Function
#
# ===============================================================================
sub GetTime {

	#	GetTimeArray(Sec[0]), Min[1], Hour[2], Day[3], Month[4], Year[5])
	my(@TimeArray) = &GetTimeArray;
	my($EXEC_TIME) = "@TimeArray[5]/@TimeArray[4]/@TimeArray[3] @TimeArray[2]:@TimeArray[1]:@TimeArray[0]";
	return($EXEC_TIME);
}

# ===============================================================================
#
#	GetTimeArray Function
#
# ===============================================================================
sub TimeDiff {

    #use Date::Manip;
    
	my $StartTime = shift;
	my $FinalTime = &GetTime; #time();
	#my $Diff = $FinalTime - $StartTime;
    my $Diff = &DateCalc($FinalTime, $StartTime);
    my $Teste = &Delta_Format($Diff, 0, "%st");
	#my $Teste = localtime($Diff);
	return($Teste);
}

# ===============================================================================
#
#	PrintHelp Function
#
# ===============================================================================
sub PrintHelp {

print "\n
# ==============================================================
# Script Help
# Usage: spliter.pl carrier service
#
# Carrier:
# 	Maxitel\t\t= mxt
# 	Tim Sul\t\t= tcs
# 	Tim Nordeste\t= tnc
#
# Service:
#	 Provisioning\t= prov
#	 Billing\t= bill
#
# Usage Examples:
#
# spliter.pl mxt prov
# (This command line will process Maxitel Provisioning files)
#
# spliter.pl tnc bill
# (This command line will process Tim Nordeste billing files)
#
# =============================================================\n\n";

}

# ===============================================================================
#
#	LogErro Function
#
# ===============================================================================
sub LogErro {
	$Err_msg = shift;
	open (ERROR_LOG,">>$Service_DIR/logs/logerro.txt") or die "Can't open logerro.txt file\n$!";
	print ERROR_LOG "$Err_msg\n$!";
	return "\n";
	open (STDERR,">$Service_DIR/logs/logerro.txt") or die "Can't write to logfile: $!\n";


}


# ===============================================================================
#
#	PCRunning Function
#
# ===============================================================================
sub PCRunning {

	my $PC_Dir = shift;
	$PC_Dir =~ s/c\://;
	$PC_Dir =~ s/\\/\//;
}


