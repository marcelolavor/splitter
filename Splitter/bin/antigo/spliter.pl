#!/usr/local/bin/perl #-w

# ===============================================================================
#	MAIN Routine
#	Usage: spliter.pl carrier service
#
#	Carrier:
#		Maxitel 	  = mxt
#		Tim Sul 	  = tcs
#		Tim Nordeste  = tnc
#
#	Service:
#		Provisioning  = prov
#		Billing		  = bill
#
#	Usage Example:
#	spliter.pl mxt prov (This command line will process Maxitel Provisioning files
#	spliter.pl tnc bill (This command line will process Tim Nordeste billing files
#
# ===============================================================================

MAIN: {

	# Get line command parameters
	#local($Carrier,$Service) = @ARGV;
	local($Carrier,$Service) = ("mxt","prov");
	chomp;
	if ($Carrier eq "") { die };		
	if ($Service eq "") { die };
	
	print "Parameters:\nCarrier: $Carrier\nService: $Service\n";
	
	# Define OS running
	# WINDOWS
	#my $slash = "\\";
	#my $drive_letter = "c:";
	# UNIX
	#my $slash = "/";
	#my $drive_letter = "";
	
	#$PC = 1;
    # Get script directory
	use strict;
	use File::Spec::Functions qw(rel2abs);
	use File::Basename;
	#$APP_DIR = dirname(rel2abs($0));
	$APP_DIR = "c:/perl/dev/splitter";
	#$PC ? &PCRunning(".."):" ;
	
	print "App DIR: $APP_DIR";
    #$APP_DIR = "c:/Perl/Dev/Splitter";

	# Define service root dir
	if (lc($Service) eq "prov") {
	   $Service_DIR = "$APP_DIR/prov/$Carrier/opr";
	} else {
	   $Service_DIR = "$APP_DIR/bill/$Carrier/rsp";	
	}

	# Define input data directory
    my($Input_Dir) = "$Service_DIR/input";

	print $Input_Dir;
	    
    # OPEN EACH ".OPR" FILE
    opendir (DIR, $Input_dir) or die "Cannot open directory: " . $Input_dir;
    foreach my $file (readdir(DIR)) {
    	if ((!-d $file) && (lc(&FileExtension($file)) eq lc("opr"))) {
    		&SplitFile($file);
    	}
    }
	# Close directory
    closedir (DIR);
}


# ===============================================================================
#
#	SplitFile Function
#
# ===============================================================================
sub SplitFile {

	# GET PARAMETER
	my($CARRIER_FILE_NAME)=shift;

	# Define output directories and files
	local $ACOTEL_FILE = "$Service_DIR/output/acotel/$CARRIER_FILE_NAME";
	local $TIMNET_FILE = "$Service_DIR/output/timnet/$CARRIER_FILE_NAME";

	my $FileName = &FileName($CARRIER_FILE_NAME);
	my $ExecTime = &GetTime;
	my $CARRIER_BACKUP_FILE_NAME = "$Service_DIR/backup/$FileName.$ExecTime";
	my $CONTROL_FILE = "$APP_DIR/control_lists/" . &Carrier . "_control_list.txt";
	
	#print "FILE NAME: $FileName\n";
	#print "ACOTEL FILE: $ACOTEL_FILE\n";
	#print "TIMNET FILE: $TIMNET_FILE\n";
	#print "EXECUTION TIME: $ExecTime\n";
	#print "BACKUP FILE: $CARRIER_BACKUP_FILE_NAME\n\n";

	open (CARRIER_FILE,$CARRIER_FILE_NAME) or die "Can't open file $CARRIER_FILE_NAME: $!";
	open (ACOTEL_FILE,">>$ACOTEL_FILE") or die "Can't open file $ACOTEL_FILE: $!";
	open (TIMNET_FILE,">>$TIMNET_FILE") or die "Can't open file $TIMNET_FILE: $!";

	while (<CARRIER_FILE>) {

        # Keep original registry 	  	
        my $OriginalCarrierLine = $_; 

		# Remove the split character from CARRIER_FILE line 	  	
		chomp;
	
		($status,$phone)=split(/\s+/);
		#print "PHONE TO CHECK: $phone\n\n";
	
		open (CONTROL_FILE,$CONTROL_FILE) or die "Can't open file $CONTROL_FILE: $!";
		#print "CONTROL PHONE LIST:\n\n";

		$FOUND = 0;
		while (<CONTROL_FILE>) {

    		# Remove the split character 	  	
			chomp;
			
			#($control_status,$control_phone)=split(/\s+/);
			#print $phone . "  " . $_ . "\n";
			if ($phone == $_) {
				$FOUND=1;
				last;
			}
		}

		#print "Found: $FOUND\n";
	
		if ($FOUND==1) {
			print ACOTEL_FILE $OriginalCarrierLine or die "Can't write to file $ACOTEL_FILE: $!";
			$FOUND = 0;
		} else {
			print TIMNET_FILE $OriginalCarrierLine or die "Can't write to file $TIMNET_FILE: $!";
		}
	
		close CONTROL_FILE;
	}
	close CARRIER_FILE or die "Can't close file $CARRIER_FILE: $!";
	close ACOTEL_FILE or die "Can't close file $ACOTEL_FILE: $!";
	close TIMNET_FILE or die "Can't close file $TIMNET_FILE: $!";
	
	
	rename($CARRIER_FILE_NAME, "$CARRIER_BACKUP_FILE_NAME") or die "Can't rename $CARRIER_FILE_NAME to $CARRIER_BACKUP_FILE_NAME: $!";
	
	#use strict;
	#use File::Copy;
	
	#my $source = "$CARRIER_BACKUP_FILE_NAME";
	#my $dest = "/backup/";
	
	#move($source, $dest) or die "Error moving file %CARRIER_BACKUP_FILE_NAME: $!\n";

}


# ===============================================================================
#
#	FileName Function
#
# ===============================================================================
sub FileName {

	# GET PARAMETER
	my($FILE_NAME)=@_;
	my($file_name,$ext) = split(/\./,$FILE_NAME);
	return($file_name);
}

# ===============================================================================
#
#	FileExtension Function
#
# ===============================================================================
sub FileExtension {

	# GET PARAMETER
	my($FILE_NAME)=@_;
	my($file_name,$ext) = split(/\./,$FILE_NAME);
	return($ext);
}


# ===============================================================================
#
#	GetTime Function
#
# ===============================================================================
sub GetTime {

	my($sec,$min,$hour,$day,$mon) = localtime($^T);
	my($EXEC_TIME) = join("",$mon+1,$day,$hour,$min,$sec);
	return($EXEC_TIME);
}


# ===============================================================================
#
#	PrintHelp Function
#
# ===============================================================================
sub PrintHelp {

print "
Missing Parameters!\n\n
# ==============================================================
# Script Help
# Usage: spliter.pl carrier service
#
# Carrier:
# 	Maxitel 	  = mxt
# 	Tim Sul 	  = tcs
# 	Tim Nordeste  = tnc
#
# Service:
#	 Provisioning  = prov
#	 Billing		  = bill
#
# Usage Example:
# spliter.pl mxt prov
# (This command line will process Maxitel Provisioning files)
#
# spliter.pl tnc bill
# (This command line will process Tim Nordeste billing files)
#
# =============================================================\n\n";

}

# ===============================================================================
#
#	PCRunning Function
#
# ===============================================================================
sub PCRunning {

	my $PC_Dir = shift;
	$PC_Dir =~ s/c\://;
	$PC_Dir =~ s/\\/\//;
}
