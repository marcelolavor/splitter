#!/usr/bin/perl
#
# bftp-put.pl   Grand Central Better FTP "PUT" script
#
#               Usage:    bftp-put.pl <Recipient's GC Address> <File>
#               Example:  bftp-put.pl TheAcmeCompany/Supply myfile.zip
#
#               Uses the Grand Central Network to send a file as a GC
#               message attachment to the specified recipient

use gc;         # Grand Central Perl SDK
use File::Basename;

gc::gcInit(1);

# SET THESE: Configuration variables
$configFile  = "gc.config";          # Your Grand Central config file
$senderId    = "012345abcde54321";   # Your Grand Central GCID

if (($#ARGV != 1)) { die "Usage: $0 <recipient's GC Address> <file>\n"; }
$recpt       = $ARGV[0];
$attachFile  = $ARGV[1];

die "ERROR:  $configFile does not exist\n" if ! -f $configFile;
die "ERROR:  $attachFile does not exist\n" if ! -f $attachFile;

$sender      = new gc::GCService();
$receiver    = new gc::GCService();
$connection  = new gc::GCConnection();

print "Sending file $attachFile to $recpt via the Grand Central Network...\n";

# Load the GC configuration file ($configFile)
$error = gc::loadConnectionConfig($connection, $configFile);
if ($error != 0) { throwGCError($error, "loadConnectionConfig"); }

$postResult             = new gc::GCPostResult();
$message                = gc::createGCMessage();
$message->{topic}       = "betterftp";
$message->{messageType} = "request";

# Set the sender value
$sender->{address}      = $senderId;
$error = gc::setSender($message, $sender);
if ($error != 0) { throwGCError($error, "setSender"); }

# Set the recipient value
$receiver->{address} = $recpt;
$error = gc::setRecipient($message, $receiver);
if ($error != 0) { throwGCError($error, "setRecipient"); }

# Create a new SOAP attachment
$att = new gc::GCAttachment();

# We'll use the file name as the doc_id
($name, $path, $suffix) = fileparse($attachFile);
$att->{doc_id} = $name . $suffix;

# Load the specified file
$error = gc::loadAttachmentData($att, $attachFile);
if ($error != 0) { throwGCError($error, "loadAttachment"); }

# Attach the file to the current message as a SOAP attachment
$error = gc::addAttachment($message, $att);
if ($error != 0) { throwGCError($error, "addAttachment"); }

# Now, send the message
$error = gc::sendGCMessage($connection, $message, $postResult);
if ($error != 0) { throwGCError($error, "sendGCMessage"); }

# Shutdown GC
gc::gcDeinit();

# Exit subroutine for obtaining extended error code information
sub throwGCError
{
   my($err, $method) = @_;
   $string = gc::getExtendedErrorInfo();
   die "ERROR: $method failed (Error $err: $string)\n";
}

